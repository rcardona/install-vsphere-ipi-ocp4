# install-vsphere-ipi-ocp4

install-vsphere-ipi-ocp4

## Getting started

#### 1- Download installer and artifacts
> wget https://mirror.openshift.com/pub/openshift-v4/x86_64/clients/ocp/stable/openshift-install-linux.tar.gz
>

> wget https://mirror.openshift.com/pub/openshift-v4/x86_64/clients/ocp/stable/openshift-client-linux.tar.gz
>

> wget https://mirror.openshift.com/pub/openshift-v4/x86_64/dependencies/rhcos/4.10/latest/rhcos-vmware.x86_64.ova
>

#### 2- Configuring the installation services

- Configure HTTP server to serve the rhcos-vmware.x86_64.ova image. This to avoid downloading the image directly from internet in case the cluster(s) need to be reinstalled. Set the location of this server and update it in the `install-config.yaml` in the following steps.

- Configure DHCP server with the IP with the allowed ranges of the VLAN, and specify the DNS server that the environment is using.

#### 3- Create ssh key
> ssh-keygen -t ed25519 -N ''
>

#### 4- Create `install-config.yaml`


    echo -e 'apiVersion: v1
    baseDomain: example.com
    compute:
    - hyperthreading: Enabled
      name: worker
      replicas: 3
    controlPlane:
      hyperthreading: Enabled
      name: master
      replicas: 3
    metadata:
      name: test
    platform:
      vsphere:
        vcenter: your.vcenter.server
        username: username
        password: password
        datacenter: datacenter
        defaultDatastore: datastore
        folder: "/<datacenter_name>/vm/<folder_name>/<subfolder_name>"
        diskType: thin
        clusterOSImage: http://kvm.doma.casa:8080/rhcos-vmware.x86_64.ova
    fips: false
    pullSecret: "{}"
    sshKey: "ssh-ed2..."
    publish: External
    proxy:                                            <===== Added
      httpProxy: http://172.16.0.1:3128/              <===== Added
      httpsProxy: http://172.16.0.1:3128/             <===== Added
      noProxy: 172.16.0.0/24,apps.vde.openshift.pub   <===== Added' | tee install-config.yaml
